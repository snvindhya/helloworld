
 import React, { useEffect } from 'react';
 import {
   Text
 } from 'react-native';
import data from "../Data/Data.json"; 

 const HelloWorld = () => {
    console.log(data,"data is fetched from the local")
   return (
      <Text style={{fontWeight:'bold',fontSize:30}}>Hello World !!</Text>
   );
 };
 
 export default HelloWorld;
 