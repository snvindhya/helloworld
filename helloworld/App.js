/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  Text,
  View,
} from 'react-native';
import HelloWorld from "./Componets/HelloWorld"


const App = () => {
  return (
   <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
    <HelloWorld/>
   </View>
  );
};

export default App;
